#FROM maven:3.6.3-openjdk-11
# RUN  mvn spring-boot:build-image -Dspring-boot.build-image.imageName=xhermesx/hello

FROM openjdk:13-jdk-alpine3.10
VOLUME /tmp
ADD /target/hello-0.0.1-SNAPSHOT.jar hello.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/hello.jar"]



